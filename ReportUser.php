<?php
require_once 'function.php'

/*include 'function.php';

function load_tab() {
	
	$queri = "SELECT * FROM user" ;

	$hasil = MySQL_query ($queri);
	
	while ($data = mysql_fetch_array ($hasil)){
	$id = $data['Username'];
	echo "    
        <tr>
			<th><a href'EditReportUser.php?id=".$data['Username']."</a></th>
			<th>".$data['FullName']."</th>
			<th>".$data['Email']."</th>
			<th>".$data['ContactPerson']."</th>
			<th>".$data['DOB']."</th>
		</tr>";        
	}
}*/
?>

<!DOCTYPE html>
<html>
<head>
	<title>Control Panel Travel Book</title>
	 <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <li><a href="Report.php">Report</a></li>
    <li><a href="account.php">Manage Account</a></li>
    <li><a href="rating.php">Ratings</a></li>
    </ul> 


	<div class="panel" style="margin-left:200px; margin-right:350px; margin-top:50px">
	    <div class="heading">
	        <span class="title">Laporan Lupa Password</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
			<table class="table striped">
		        <tr>
		        	<th>Username</th>
		        	<th>Fullname</th>
		        	<th>Email</th>
		        	<th>Contact Person</th>
		        	<th>Date Of Birth</th>
		        </tr>
				<?php  
					$hasil=ambilSemuaDataUser();
					foreach ($hasil as $data) {
					 	echo "    
					        <tr>
							<th><a href='EditReportUser.php?id=".$data['Username']."'>".$data['Username']."</a></th>
					        <th>".$data['FullName']."</th>
					        <th>".$data['Email']."</th>
					        <th>".$data['ContactPerson']."</th>
							<th>".$data['DOB']."</th>
					        </tr>";
					}
				?>	
	        </table>
   	    </div>
	</div>
</body>
</html>