<?php
error_reporting(0);
include 'db-connection.php';

function load_tabelPost() {
	// Perintah untuk menampilkan data
$queri="SELECT * FROM user ORDER BY NumberOfPost DESC" ;  //menampikan SEMUA data dari tabel siswa

$hasil=MySQL_query ($queri);    //fungsi untuk SQL

// perintah untuk membaca dan mengambil data dalam bentuk array
while ($data = mysql_fetch_array ($hasil)){
$id = $data['id'];
 echo "    
        <tr>
        <td>".$data['Username']."</td>
        <td>".$data['FullName']."</td>
        <td>".$data['NumberOfPost']."</td>
        </tr> 
        ";
}
}

function load_tabelRate() {
	// Perintah untuk menampilkan data
$queri="SELECT * FROM user_lastrate ORDER BY Rating DESC" ;  //menampikan SEMUA data dari tabel siswa

$hasil=MySQL_query ($queri);    //fungsi untuk SQL

// perintah untuk membaca dan mengambil data dalam bentuk array
while ($data = mysql_fetch_array ($hasil)){
$id = $data['id'];
 echo "    
        <tr>
        <td>".$data['Username']."</td>
        <td>".$data['Rating']."</td>
        </tr> 
        ";
}
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Control Panel Travel Book</title>
	 <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="#">Travel Book</a></li>
    <li><a href="#">Home</a></li>
    <li><a href="#">Report</a></li>
    <li><a href="#">Manage Account</a></li>
    </ul> 

	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Top Rated Users</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>Username</th>
		        	<th>Rating</th>
		        </tr>
	        </table>
   	    </div>
	</div>
	
	<?php
		load_tabelRate();
	?>
	
	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Most Active Post</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>Username</th>
		        	<th>Fullname</th>
		        	<th>Post</th>
		        </tr>
	        </table>
   	    </div>
	</div>
	
	<?php
		load_tabelPost();
	?>
</body>
</html>