<?php

require_once 'function.php';
checkLogin();
$userid = $_SESSION['user']['Username'];
?>

<html>
<head>
	<title><?= $_SESSION['user']['FullName']; ?></title>
     <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <?
    $level = getLevel($_SESSION['user']['Username']);
    if ($level['Level'] != 5){
				echo "<li><a href='index.php'>Back End</a></li>";
			}
    ?>
    <li>
                    
    </li>
    <li class="place-right no-hovered">
        <a href="#" class="dropdown-toggle">Profile</a>
            <ul  class="d-menu place-right no-margin-top block-shadow" data-role="dropdown">
	            <li><a href="#">Profile</a></li>
	            <li><a href="#">Settings</a></li>
            </ul>
    </li>
</ul> 

<div id="container">
		<div id="profile">
			<div class="tile-small bg-transparent fg-white" data-role="tile" style="width: 150px; height:150px;">
				<div class="tile-content"></div>
				<div class="image-container image-format-square" style="width: 100%;">
					<div class="frame">
						<div style="width: 100%; height: 100%; background-image: url('img/profile.jpg'); background-size: cover; background-position:center ;background-repeat: no-repeat; border-radius: 0px;-moz-border-radius: 75px;-webkit-border-radius: 75px;">
						</div>
					</div>
				</div>
			</div>
			<div id="profile-name">
				<h1><?= $_SESSION['user']['FullName']; ?></h1>
			</div>
		</div>
		<br>
		<div id="contents">
			<div class="tile bg-transparent fg-white" data-role="tile" style="width: 250px; height:250px;">
				<div class="tile-content"></div>
				<div class="image-container image-format-square" style="width: 100%;">
					<div class="frame">
						<div style="width: 100%; height: 100%; background-image: url('img/DSCF0389_Raw_4a.jpg'); background-size: cover; background-position:center ;background-repeat: no-repeat; border-radius: 0px;">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>          
</body>
</html>