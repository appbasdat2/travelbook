<?php

require_once 'function.php';
checkLogin();
$userid = $_SESSION['user']['Username'];

if(isset($_POST['doLogOut'])){
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Control Panel Travel Book</title>
	 <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <li><a href="Report.php">Report</a></li>
    <li><a href="account.php">Manage Account</a></li>
    <li><a href="rating.php">Ratings</a></li>
    <li class="place-right no-hovered">
        <a href="#" class="dropdown-toggle">Profile</a>
            <ul  class="d-menu place-right no-margin-top block-shadow" data-role="dropdown">
	            <li><a href="#">Profile</a></li>
	            <li><a href="home.php" name="doLogOut">Log Out</a></li>
            </ul>
    </li>
    </ul> 


	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Reported Post</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>POST ID</th>
		        	<th>DATE</th>
		        	<th>REPORTER</th>
		        	<th>CLAUSE</th>
		        	<th>COMMENT</th>
		        	<th>STATUS</th>
		        	<th>ACTION</th>
		        	<th>TAKE ACTION</th>
		        </tr>
				<?php 	        
					if(isset($_POST['doRemove'])){
						$value = $_POST['doRemove'];
						echo "<script type='text/javascript'>alert('post telah di proses');</script>";
						removePost($value);
					}
					$hasil=ambilSemuaDataReport();
					foreach ($hasil as $data) {
						if ($data['Status']=="Processed") {
							echo "    
					        <tr>
					        <th>".$data['PostID']."</td>
							<th>".$data['Date']."</td>
					        <th>".$data['Username']."</td>
					        <th>".$data['Conduct']."</td>
					        <th>".$data['Comment']."</td>
							<th>".$data['Status']."</td>
							<th>DONE</td>
					        </tr>";
						}
						else{
							echo "    
					        <tr>
					        <th>".$data['PostID']."</td>
							<th>".$data['Date']."</td>
					        <th>".$data['Username']."</td>
					        <th>".$data['Conduct']."</td>
					        <th>".$data['Comment']."</td>
							<th>".$data['Status']."</td>
							<th><form method='post'>
								<button type='submit' name='doRemove' value='".$data['PostID']."'>Remove</button>
							</form></td>
					        </tr>";
						}
					}
				?>
	        </table>
   	    </div>
	</div>

	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Trip Rating</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
			<table class="table striped">
			<table class="table sortable-markers-on-left">
		        <tr>
		        	<th class="sortable-column">TRIP ID</th>
					<th class="sortable-column sort-asc">USERNAME</th>
					<th class="sortable-column sort-desc">CITY</th>
					<th>DESCRIPTION</th>
					<th>EXPENSE</th>
					<th>TRIP DATE</th>
					<th>TRIP RATING</th>
<?php  
$hasil=ambilSemuaDataTrip();
foreach ($hasil as $data) {
 	 echo "    
        <tr>
        <th>".$data['TripID']."</td>
		<th>".$data['Username']."</td>
        <th>".$data['City']."</td>
        <th>".$data['Description']."</td>
        <th>".$data['Expense']."</td>
		<th>".$data['TripDate']."</td>
        ";
        $tripID = $data['TripID'];
        $RatingTotal = countRating($tripID);
        echo"<th>".$RatingTotal."</td></tr>";
}
?>	
			</table>
			</table>
			</table>
			<br>
			 <table class="report-table">
			 <table class="table sortable-markers-on-left">
		        <tr>
				
		        	<th class="sortable-column">TRIP ID</th>
					<th class="sortable-column sort-asc">USERNAME</th>
					<th class="sortable-column sort-desc">RATING</th>
					<th>COMMENT</th>
					
					
<?php  
$hasil2=ambilSemuaDataRating();
foreach ($hasil2 as $data) {
 	echo "    
        <tr>
        <th>".$data['TripID']."</td>
		<th>".$data['Username']."</td>
        <th>".$data['Rating']."</td>
        <th>".$data['Comment']."</td>

        </tr> 
        ";
        
}

?>
			</table>
			</table>
   	    </div>
	</div>

	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Last Rated Users</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>Username</th>
		        	<th>Rating</th>
		        	<th>Post ID</th>
		        </tr>	        
				<?php
					$hasil=load_tabelRate();
					foreach ($hasil as $data) {
					 	 echo "    
					        <tr>
					        <th>".$data['Username']."</td>
							<th>".$data['Rating']."</td>
							<th>".$data['PostID']."</td>
					        </tr>";
					}
				?>
	        </table>
   	    </div>
	</div>
	
	
	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Most Post</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>Username</th>
		        	<th>Fullname</th>
		        	<th>Post</th>
		        </tr>
				<?php
					$hasil=load_tabelPost();
					foreach ($hasil as $data) {
					 	 echo "    
					        <tr>
					        <th>".$data['Username']."</td>
							<th>".$data['FullName']."</td>
							<th>".$data['NumberOfPost']."</td>
					        </tr>";
					}
				?>
	        </table>
   	    </div>
	</div>

	<br>
	<br>
	
</body>
</html>