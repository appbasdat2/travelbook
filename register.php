<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<title>Travel Book</title>
<link href="bootstrap.css" rel="stylesheet" type="text/css">
<link href="coba1.css" rel="stylesheet" type="text/css">
<link href="bootstrap.min.css" rel="stylesheet" type="text/css">
</head>

<body background="white">
<div class="Container">
<div class="tint">
	<nav class="navbar navbar-default">
	<div class="dropdown1" style="float:right;">
		<button class="dropbtn">Travel Book Info</button> 
		<div class="dropdown-content1">
		<a href="home.html">Home</a>
		<a href="help.html">Help</a>
		<a href="about.html">About</a>
		</div>
	</div>
		<div class="container-fluid"> 
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#defaultNavbar1"><span class="sr-only">Toggle navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
				<div class="dropdown">
				<br>
  <span>Travel Book</span>
  <div class="dropdown-content">
    <p>Travel Book</p>
  </div>
  
</div>
			
			
	</nav>

<div class="Auth1">
        <div class="OuterAccountBox">
			<div class="Account">
				<form action="" method="post" name="SignInForm" id="SignInForm">
				 <table border="0" align="center">
					<tbody>
					 <p class="font">Register</p>
					 <tr>
					  <td align="left" valign="top">
					   <h6>Username:<br><input name="Username" type="text" required="required" class="TextFieldStyle" id="Username">
					   </h6>
					  </td>
					 </tr>
					 
					 <tr>
					  <td align="left" valign="top">
					   <h6>Full Name:<br><input name="fullname" type="text" required="required" class="TextFieldStyle" id="Full Name">
					   </h6>
					  </td>
					 </tr>
					 
					 <tr>
					  <td align="left" valign="top">
					   <h6>Email:<br><input name="email" type="text" required="required" class="TextFieldStyle" id="Email">
					   </h6>
					  </td>
					 </tr>
					 
					 
					 
					 <tr>
					  <td align="left" valign="top">
					   <h6>Password:<br><input name="password" type="password" required="required" class="TextFieldStyle" id="Password">
					   </h6>
					  </td>
					 </tr>
					 
					 <tr>
					  <td align="left" valign="top">
					   <h6>Contact Person:<br><input name="contact" type="contact" required="required" class="TextFieldStyle" id="Contact Person">
					   </h6>
					  </td>
					 </tr>
					 
					 <tr>
					  <td align="right" valign="top"><input name="register" type="submit" class="Button" id="register" value="Create Account"></td>
					 </tr>
					</tbody>
				 </table>
				</form>
			</div>
        </div>		  
    </div>
		<div class="navbar-footer">
		<div class="text-center col-md-6 col-md-offset-3">

	<hr>
		 <p>Copyright &copy; 2016 &middot; All Rights Reserved &middot; Travel Books</p>
		</div>
		</div>
	</div>
	</div>

<script src="jquery-1.11.2.min.js"></script>
<script src="bootstrap.js"></script>
</body>
</html>