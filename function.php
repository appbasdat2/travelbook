<?php

session_start();
require_once 'db-connection.php';

function login($username, $password){
	if((trim($username) !== "")&&(trim($password) !== "")){
		$query = "select * from User where username = '$username' and password = '$password'";
		$result = ambilDataBaris($query);
		if(!empty($result)){
			unset($result['password']);
			$_SESSION['user'] = $result;
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

function getLevel($username){
	if((trim($username) !== "")){
		$query = "select Level from User where username = '$username'";
		return ambilDataBaris($query);
	}
}

function countRating($tripID){
	$query = "select count(*) as cnt from trip_rating where TripID = '$tripID'";
	$hasil = ambilDataBaris($query);
	$query = "select * from trip_rating where TripID = '$tripID'";
	$hasil2 = ambilDataSemua($query);
	$sum = 0;
	foreach ($hasil2 as $data) {
		$sum = $sum + $data['Rating'];
	}
	$rata2 = $sum / $hasil['cnt'];
	return $rata2;
}

function ambilSemuaDataReport(){
	$query = "select * from post_report";
	return ambilDataSemua($query);
}

function ambilSemuaDataTrip(){
	$query = "select * from trip";
	return ambilDataSemua($query);
}

function ambilSemuaDataRating(){
	$query = "select * from trip_rating";
	return ambildatasemua($query);
}

function register($username,$nama,$password){
	$query = "insert into users (username,nama,password)
			   values('$username','$nama','$password')";
	return execute($query);
}

function save($messageid,$from,$to,$subject,$content){
	if(intval($messageid) == 0){
		$query = "insert into messages (`from`,`to`,`subject`,content,created_ts)
				   values($from,$to,'$subject','$content',current_timestamp)";
   	} else {
   		$query = "update messages set `to` = $to,`subject` = '$subject',content = '$content',updated_ts = current_timestamp where messageid = $messageid";
   	}
	return execute($query);
}


function send($from,$to,$subject,$content){
	$query = "insert into messages (`from`,`to`,`subject`,content,terkirim,created_ts,sent_ts) 
			   values ($from,$to,'$subject','$content','Y',current_timestamp,current_timestamp)";
	return execute($query);
}

function getUsers($userid){
	$query = "select username,fullname from user where username != $userid order by nama asc";
	return ambilDataSemua($query);
}

function checkUsernameExists($username){
	$query = "select count(*) as cnt from users where username = '$username'";
	$result = ambilDataBaris($query);
	return ($result['cnt'] > 0);
}

function checkLogin(){
	if(!isset($_SESSION['user'])){
		clearSession();
		header("Location: " . str_replace(basename($_SERVER['PHP_SELF']),'',$_SERVER['PHP_SELF']));
	}
}

function clearSession(){
	unset($_SESSION['user']);
}

function load_tabelPost() {
	// Perintah untuk menampilkan data
$queri="SELECT * FROM user ORDER BY NumberOfPost DESC" ; 

return ambilDataSemua($queri);
}

function load_tabelRate() {
	// Perintah untuk menampilkan data
$queri="SELECT * FROM user_lastrate ORDER BY Rating DESC" ;  

return ambilDataSemua($queri);
}

function removePost($postid){
	$query = "update post_report set status='Processed', action='Remove Post' where postid='$postid'";
	return execute($query);
}

