<?php
require_once 'function.php'
?>

<!DOCTYPE html>
<html>
<head>
	<title>Control Panel Travel Book</title>
	 <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <li><a href="Report.php">Report</a></li>
    <li><a href="account.php">Manage Account</a></li>
    <li><a href="rating.php">Ratings</a></li>
    </ul> 

	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Last Rated Users</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>Username</th>
		        	<th>Rating</th>
		        	<th>Post ID</th>
		        </tr>	        
				<?php
					$hasil=load_tabelRate();
					foreach ($hasil as $data) {
					 	 echo "    
					        <tr>
					        <th>".$data['Username']."</td>
							<th>".$data['Rating']."</td>
							<th>".$data['PostID']."</td>
					        </tr>";
					}
				?>
	        </table>
   	    </div>
	</div>
	
	
	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Most Post</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>Username</th>
		        	<th>Fullname</th>
		        	<th>Post</th>
		        </tr>
				<?php
					$hasil=load_tabelPost();
					foreach ($hasil as $data) {
					 	 echo "    
					        <tr>
					        <th>".$data['Username']."</td>
							<th>".$data['FullName']."</td>
							<th>".$data['NumberOfPost']."</td>
					        </tr>";
					}
				?>
	        </table>
   	    </div>
	</div>
	
</body>
</html>