<?php

require_once 'function.php'

?>
<!DOCTYPE html>
<html>
<head>
	<title>Travel Book Trip Rating</title>
	<link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <li><a href="Report.php">Report</a></li>
    <li><a href="account.php">Manage Account</a></li>
    <li><a href="rating.php">Ratings</a></li>
	<li><a href="user.php">User</a></li>
    </ul> 


	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Daftar User</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
			<table class="table striped">
			<table class="table sortable-markers-on-left">
		        <tr>
		        	<th>USERNAME</th>
					<th>PASSWORD</th>
					<th>CONFIRM PASSWORD</th>
					<th>FULLNAME</th>
					<th>EMAIL</th>
					<th>CONTACT PERSON</th>
					<th>STATUS</th>
					<th>PIN</th>
					<th>NUMBER OF POST</th>
					<th>DOB</th>
					
					
<?php
$hasil=ambilSemuaDataUser();  
foreach ($hasil as $data) {
 	 echo "    
        <tr>
        <th>".$data['Username']."</td>
		<th>".$data['Password']."</td>
		<th>".$data['ConfirmPassword']."</td>
        <th>".$data['FullName']."</td>
        <th>".$data['Email']."</td>
        <th>".$data['ContactPerson']."</td>
		<th>".$data['Status']."</td>
		<th>".$data['PIN']."</td>
		<th>".$data['NumberOfPost']."</td>
		<th>".$data['DOB']."</td>
        ";
      
}
?>	
		</table>
					</table>
					</table>
   	    </div>
	</div>
</body>
</html>