<?php

$host = "localhost";
$user = "root";
$pwd = "";
$db = "TravelBook";
$koneksi = mysqli_connect($host,$user,$pwd,$db);

if(!$koneksi) die("Koneksi Database Gagal.");

function ambilDataSemua($query){
	global $koneksi;
	$stmt = mysqli_query($koneksi,$query);
	$result = array();
	while($row = mysqli_fetch_assoc($stmt)){
		$result[] = $row;
	}
	mysqli_free_result($stmt);
	return $result;
}

function ambilDataBaris($query){
	global $koneksi;
	$stmt = mysqli_query($koneksi,$query);
	$result = mysqli_fetch_assoc($stmt);
	mysqli_free_result($stmt);
	return $result;
}

function execute($query){
	global $koneksi;
	$stmt = mysqli_query($koneksi,$query);
	return true;
}

function showError(){
	echo mysqli_connect_error();
}