<?php

require_once 'function.php';

?>

<!DOCTYPE html>
<html>
<head>
	<title>Travel Book Trip Rating</title>
	 <link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <li><a href="Report.php">Report</a></li>
    <li><a href="account.php">Manage Account</a></li>
    <li><a href="rating.php">Ratings</a></li>
    </ul> 


	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Trip Rating</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
		        <tr>
		        	<th>TRIP ID</th>
					<th>DATE</th>
					<th>USERNAME</th>
					<th>CONDUCT</th>
					<th>COMMENT</th>
					<th>STATUS</th>
		        	<th>ACTION</th>
		        </tr>
		        <?php 	        
					if(isset($_POST['doRemove'])){
						$value = $_POST['doRemove'];
						echo "<script type='text/javascript'>alert('post telah di proses');</script>";
						removePost($value);
					}
					$hasil=ambilSemuaDataReport();
					foreach ($hasil as $data) {
						if ($data['Status']=="Processed") {
							echo "    
					        <tr>
					        <th>".$data['PostID']."</td>
							<th>".$data['Date']."</td>
					        <th>".$data['Username']."</td>
					        <th>".$data['Conduct']."</td>
					        <th>".$data['Comment']."</td>
							<th>".$data['Status']."</td>
							<th>DONE</td>
					        </tr>";
						}
						else{
							echo "    
					        <tr>
					        <th>".$data['PostID']."</td>
							<th>".$data['Date']."</td>
					        <th>".$data['Username']."</td>
					        <th>".$data['Conduct']."</td>
					        <th>".$data['Comment']."</td>
							<th>".$data['Status']."</td>
							<th><form method='post'>
								<button type='submit' name='doRemove' value='".$data['PostID']."'>Remove</button>
							</form></td>
					        </tr>";
						}
					}
				?>	
	        </table>
   	    </div>
	</div>
</body>
</html>