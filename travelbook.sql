-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 25, 2016 at 04:52 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `travelbook`
--

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

CREATE TABLE IF NOT EXISTS `comment` (
  `CommentID` varchar(17) NOT NULL,
  `Comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE IF NOT EXISTS `post` (
  `PostID` varchar(15) NOT NULL,
  `PostDesc` text NOT NULL,
  `PhotoURL` text NOT NULL,
  `Post_Date` date NOT NULL,
  `LastEdit_Date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post`
--

INSERT INTO `post` (`PostID`, `PostDesc`, `PhotoURL`, `Post_Date`, `LastEdit_Date`) VALUES
('1234', 'asdf', '', '2016-10-25', '2016-10-25');

-- --------------------------------------------------------

--
-- Table structure for table `post_comment`
--

CREATE TABLE IF NOT EXISTS `post_comment` (
  `PostID` varchar(15) NOT NULL,
  `CommentID` varchar(17) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_report`
--

CREATE TABLE IF NOT EXISTS `post_report` (
  `Username` varchar(30) NOT NULL,
  `PostID` varchar(15) NOT NULL,
  `Conduct` text NOT NULL,
  `Comment` text NOT NULL,
  `Status` varchar(100) NOT NULL,
  `Date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Action` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `post_report`
--

INSERT INTO `post_report` (`Username`, `PostID`, `Conduct`, `Comment`, `Status`, `Date`, `Action`) VALUES
('test1234', '1234', 'Inappropriate', 'Tidak sesuai EULA', 'Not Processed', '2016-10-25 19:14:20', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `recommendation`
--

CREATE TABLE IF NOT EXISTS `recommendation` (
  `RecommendationID` varchar(14) NOT NULL,
  `Place` text NOT NULL,
  `Type` varchar(100) NOT NULL,
  `Location` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trip`
--

CREATE TABLE IF NOT EXISTS `trip` (
  `TripID` varchar(12) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `City` varchar(30) NOT NULL,
  `Description` text NOT NULL,
  `Expense` bigint(20) NOT NULL,
  `TripDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trip`
--

INSERT INTO `trip` (`TripID`, `Username`, `City`, `Description`, `Expense`, `TripDate`) VALUES
('T001', 'admin', 'Jawa Timur', 'Gunung Bromo', 100, '2016-10-01'),
('T002', 'juju', 'Malang ', 'Pantai Ngliyep', 300, '2016-10-08');

-- --------------------------------------------------------

--
-- Table structure for table `trip_post`
--

CREATE TABLE IF NOT EXISTS `trip_post` (
  `TripID` varchar(12) NOT NULL,
  `PostID` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `trip_rating`
--

CREATE TABLE IF NOT EXISTS `trip_rating` (
  `TripID` varchar(12) NOT NULL,
  `Username` varchar(30) NOT NULL,
  `Rating` int(11) NOT NULL,
  `Comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `trip_rating`
--

INSERT INTO `trip_rating` (`TripID`, `Username`, `Rating`, `Comment`) VALUES
('T001', 'mamas', 5, 'kurang lengkap info nya gan'),
('T002', 'papah', 10, 'lengkap banget infonya, jadi pengen kesana. suwun gan');

-- --------------------------------------------------------

--
-- Table structure for table `trip_recommendation`
--

CREATE TABLE IF NOT EXISTS `trip_recommendation` (
  `TripID` varchar(12) NOT NULL,
  `RecommendationID` varchar(14) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `Username` varchar(30) NOT NULL,
  `Password` varchar(30) NOT NULL,
  `FullName` varchar(60) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `ContactPerson` varchar(20) NOT NULL,
  `Status` text NOT NULL,
  `PIN` int(6) NOT NULL,
  `NumberOfPost` int(11) NOT NULL,
  `DOB` date NOT NULL,
  `Level` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Username`, `Password`, `FullName`, `Email`, `ContactPerson`, `Status`, `PIN`, `NumberOfPost`, `DOB`, `Level`) VALUES
('admin', 'admin', 'admin', 'admin@gmail.com', 'admin', 'admin', 111111, 0, '2016-10-01', 5),
('cliftjandra', 'cliftjandra', 'Clif Tjandra', 'clif.tjandra@gmail.com', '11111', '', 121212, 0, '1995-12-18', 1),
('test1234', 'test1234', 'Tester', 'test@gmail.com', '111111', '', 123456, 1, '2016-10-03', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_friend`
--

CREATE TABLE IF NOT EXISTS `user_friend` (
  `Username_Sender` varchar(30) NOT NULL,
  `Username_Recipent` varchar(30) NOT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_lastrate`
--

CREATE TABLE IF NOT EXISTS `user_lastrate` (
  `Username` varchar(30) NOT NULL,
  `Last_Rating` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `Rating` int(100) NOT NULL,
  `PostID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_lastrate`
--

INSERT INTO `user_lastrate` (`Username`, `Last_Rating`, `Rating`, `PostID`) VALUES
('test1234', '2016-10-25 19:23:52', 10, 1234);

-- --------------------------------------------------------

--
-- Table structure for table `user_post`
--

CREATE TABLE IF NOT EXISTS `user_post` (
  `Username` varchar(30) NOT NULL,
  `PostID` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_trip`
--

CREATE TABLE IF NOT EXISTS `user_trip` (
  `Username` varchar(30) NOT NULL,
  `Trip_ID` varchar(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`CommentID`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`PostID`);

--
-- Indexes for table `post_comment`
--
ALTER TABLE `post_comment`
  ADD PRIMARY KEY (`PostID`,`CommentID`);

--
-- Indexes for table `post_report`
--
ALTER TABLE `post_report`
  ADD PRIMARY KEY (`Username`,`PostID`);

--
-- Indexes for table `recommendation`
--
ALTER TABLE `recommendation`
  ADD PRIMARY KEY (`RecommendationID`);

--
-- Indexes for table `trip`
--
ALTER TABLE `trip`
  ADD PRIMARY KEY (`TripID`);

--
-- Indexes for table `trip_post`
--
ALTER TABLE `trip_post`
  ADD PRIMARY KEY (`TripID`,`PostID`);

--
-- Indexes for table `trip_recommendation`
--
ALTER TABLE `trip_recommendation`
  ADD PRIMARY KEY (`TripID`,`RecommendationID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Username`);

--
-- Indexes for table `user_friend`
--
ALTER TABLE `user_friend`
  ADD PRIMARY KEY (`Username_Sender`,`Username_Recipent`);

--
-- Indexes for table `user_lastrate`
--
ALTER TABLE `user_lastrate`
  ADD PRIMARY KEY (`Username`,`PostID`);

--
-- Indexes for table `user_post`
--
ALTER TABLE `user_post`
  ADD PRIMARY KEY (`Username`,`PostID`);

--
-- Indexes for table `user_trip`
--
ALTER TABLE `user_trip`
  ADD PRIMARY KEY (`Username`,`Trip_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
