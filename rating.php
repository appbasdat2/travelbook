<?php

require_once 'function.php'

?>
<!DOCTYPE html>
<html>
<head>
	<title>Travel Book Trip Rating</title>
	<link href="css/metro.css" rel="stylesheet">
    <link href="css/metro-icons.css" rel="stylesheet">
    <link href="css/metro-responsive.css" rel="stylesheet">
    <link href="css/metro-schemes.css" rel="stylesheet">

    <link href="css/docs.css" rel="stylesheet">

    <script src="js/jquery-2.1.3.min.js"></script>
    <script src="js/metro.js"></script>
    <script src="js/docs.js"></script>
    <script src="js/prettify/run_prettify.js"></script>
    <script src="js/ga.js"></script>
</head>
<body>
	<ul class="h-menu block-shadow-impact">
    <li><a href="home.php">Travel Book</a></li>
    <li><a href="index.php">Home</a></li>
    <li><a href="Report.php">Report</a></li>
    <li><a href="account.php">Manage Account</a></li>
    <li><a href="rating.php">Ratings</a></li>
    </ul> 


	<div class="panel" style="margin-left:50px; margin-right:50px; margin-top:100px">
	    <div class="heading">
	        <span class="title">Trip Rating</span>
	    </div>
	    <div class="content">
	        <table class="report-table">
			<table class="table striped">
			<table class="table sortable-markers-on-left">
		        <tr>
		        	<th class="sortable-column">TRIP ID</th>
					<th class="sortable-column sort-asc">USERNAME</th>
					<th class="sortable-column sort-desc">CITY</th>
					<th>DESCRIPTION</th>
					<th>EXPENSE</th>
					<th>TRIP DATE</th>
					<th>TRIP RATING</th>
<?php  
$hasil=ambilSemuaDataTrip();
foreach ($hasil as $data) {
 	 echo "    
        <tr>
        <th>".$data['TripID']."</td>
		<th>".$data['Username']."</td>
        <th>".$data['City']."</td>
        <th>".$data['Description']."</td>
        <th>".$data['Expense']."</td>
		<th>".$data['TripDate']."</td>
        ";
        $tripID = $data['TripID'];
        $RatingTotal = countRating($tripID);
        echo"<th>".$RatingTotal."</td></tr>";
}
?>	
			</table>
			</table>
			</table>
			<br>
			 <table class="report-table">
			 <table class="table sortable-markers-on-left">
		        <tr>
				
		        	<th class="sortable-column">TRIP ID</th>
					<th class="sortable-column sort-asc">USERNAME</th>
					<th class="sortable-column sort-desc">RATING</th>
					<th>COMMENT</th>
					
					
<?php  
$hasil2=ambilSemuaDataRating();
foreach ($hasil2 as $data) {
 	echo "    
        <tr>
        <th>".$data['TripID']."</td>
		<th>".$data['Username']."</td>
        <th>".$data['Rating']."</td>
        <th>".$data['Comment']."</td>

        </tr> 
        ";
        
}

?>
			</table>
			</table>
   	    </div>
	</div>
</body>
</html>